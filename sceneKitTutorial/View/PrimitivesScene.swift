//
//  PrimitivesScene.swift
//  sceneKitTutorial
//
//  Created by Roman KYSLYY on 11/13/18.
//  Copyright © 2018 Roman KYSLYY. All rights reserved.
//

import UIKit
import SceneKit

enum Disk {
    case big
    case medium
    case small
}

enum Peg {
    case first
    case second
    case third
}

let diskCount = 5

//Disks
let diskRadius : CGFloat = 1.0
let diskHeight : CGFloat = 0.2

//Board
let boardPadding : CGFloat = 0.8
let boardWidth : CGFloat = diskRadius * 6 + boardPadding
let boardLength : CGFloat = diskRadius * 2 + boardPadding
let boardHeight : CGFloat = 0.2

//Pegs
let pegRadius : CGFloat = 0.1
let pegHeight : CGFloat = 2

let initCoordinates : (CGFloat, CGFloat, CGFloat) = (diskRadius * -2, diskHeight, 0.0)

class PrimitivesScene: SCNScene {
    
    //Disks
    
    var firstPegDisks = [SCNNode]()
    var secondPegDisks = [SCNNode]()
    var thirdPegDisks = [SCNNode]()
    //------------------------
    
    //Solver
    var solver = HanoiSolver()
    var targetArray : [SCNNode]? = nil
    var targetEnum : Peg? = nil
    //
    
    override init() {
        super.init()
     
        //Initialize scene
        generateBoard()
        generatePegs()
        generateDisks()
        solver = HanoiSolver(smallestDisk: firstPegDisks.last!)
        //-------------
        
        firstStep {
            self.createNewTarget()
        }
    }
    
    func createNewTarget() {
        let result = solver.findSmallestOnTopAndDest(firstPeg: firstPegDisks, secondPeg: secondPegDisks, thirdPeg: thirdPegDisks)
        let from = result.0
        let to = result.1
        
        targetEnum = to
        
        move(from: from, to: to) {
            switch to {
            case .first: self.targetArray = self.firstPegDisks
            case .second: self.targetArray = self.secondPegDisks
            case .third: self.targetArray = self.thirdPegDisks
            }
            
            if self.solver.isTargetClosed(target: self.targetArray!) {
                self.createNewTarget()
            } else {
                self.dealWithOpenTarget()
            }
        }
    }
    
    func dealWithOpenTarget() {
        var otherOne : ([SCNNode], Peg)
        var otherTwo : ([SCNNode], Peg)

        if targetEnum! == .first {
            otherOne.0 = self.secondPegDisks
            otherOne.1 = .second
            otherTwo.0 = self.thirdPegDisks
            otherTwo.1 = .third
        } else if targetEnum! == .second {
            otherOne.0 = self.firstPegDisks
            otherOne.1 = .first
            otherTwo.0 = self.thirdPegDisks
            otherTwo.1 = .third
        } else {
            otherOne.0 = self.firstPegDisks
            otherOne.1 = .first
            otherTwo.0 = self.secondPegDisks
            otherTwo.1 = .second
        }
        self.targetArray = tArray(peg: targetEnum!)
        let result = self.solver.getPegAndAmountOnTop(targetRadius: (targetArray!.last!.geometry as! SCNTube).outerRadius, otherOne: otherOne, otherTwo: otherTwo)
        let peg = result.peg
        let amount = result.amount
        if amount == 0 {
            move(from: peg, to: targetEnum!) {
                if self.solver.isTargetClosed(target: self.tArray(peg: self.targetEnum!)!) {
                    self.createNewTarget()
                } else {
                    self.dealWithOpenTarget()
                }
            }
        } else {
            dealWithDeepDisk(from: peg, amount: amount)
        }
    }
    
    func dealWithDeepDisk(from : Peg, amount : Int) {
        var to : Peg
        if from == .first {
            if targetEnum! == .second {
                to = .third
            } else {
                to = .second
            }
        } else if from == .second {
            if targetEnum! == .first {
                to = .third
            } else {
                to = .first
            }
        } else {
            to = (targetEnum! == .first) ? .second : .first
            if targetEnum! == .first {
                to = .second
            } else {
                to = .first
            }
        }
        if amount % 2 == 0 {
            self.move(from: from, to: targetEnum!) {
                self.move(from: from, to: to, completionHandler: {
                    self.targetEnum = to
//                    print(self.targetEnum!)
                    if self.solver.isTargetClosed(target: self.tArray(peg: self.targetEnum!)!) {
                        self.createNewTarget()
                    } else {
                        self.dealWithOpenTarget()
                    }
                })
            }
        } else {
            self.move(from: from, to: to) {
                self.move(from: from, to: self.targetEnum!, completionHandler: {
//                    print(self.targetEnum!)
                    if self.solver.isTargetClosed(target: self.tArray(peg: self.targetEnum!)!) {
                        self.createNewTarget()
                    } else {
                        self.dealWithOpenTarget()
                    }
                })
            }
        }
    }
    
    //Movement
    
    func firstStep(completionHandler : @escaping () -> Void) {
        if firstPegDisks.count % 2 == 0 {
            move(from: .first, to: .second, completionHandler: {
                completionHandler()
            })
        } else {
            move(from: .first, to: .third, completionHandler: {
                completionHandler()
            })
        }
    }
    
    func move(from : Peg, to : Peg, completionHandler : (() -> Void)?) {
        var topDisk : SCNNode
//        print("From \(from) to \(to)")
        
        switch from {
        case .first: topDisk = firstPegDisks.popLast()!
        case .second: topDisk = secondPegDisks.popLast()!
        case .third: topDisk = thirdPegDisks.popLast()!
        }
        
        var yToRaise = CGFloat()
        var xToMove = CGFloat()
        var yToDrop = CGFloat()
        
        calculateDirections(yToRaise: &yToRaise, xToMove: &xToMove, yToDrop: &yToDrop, from: from, to: to)
        
        let moveUp = SCNAction.moveBy(x: 0.0, y: yToRaise, z: 0.0, duration: 0.3)
        let moveToPeg = SCNAction.moveBy(x: xToMove, y: 0.0, z: 0.0, duration: 0.3)
        let moveDown = SCNAction.moveBy(x: 0.0, y: yToDrop, z: 0.0, duration: 0.3)
        let sequence = SCNAction.sequence([moveUp, moveToPeg, moveDown])
        
        switch to {
        case .first: firstPegDisks.append(topDisk)
        case .second: secondPegDisks.append(topDisk)
        case .third: thirdPegDisks.append(topDisk)
        }
        
        DispatchQueue.main.async {
            topDisk.runAction(sequence, completionHandler: completionHandler)
        }
    }
    
    func calculateDirections(yToRaise : inout CGFloat, xToMove : inout CGFloat, yToDrop : inout CGFloat, from : Peg, to : Peg) {
        if from == .first && to == .second {
            yToRaise = pegHeight + 0.5 - CGFloat(firstPegDisks.count + 1) * diskHeight
            xToMove = diskRadius * 2
            yToDrop = -1 * (pegHeight + 0.5 - CGFloat(secondPegDisks.count + 1) * diskHeight)
        } else if from == .first && to == .third {
            yToRaise = pegHeight + 0.5 - CGFloat(firstPegDisks.count + 1) * diskHeight
            xToMove = diskRadius * 4
            yToDrop = -1 * (pegHeight + 0.5 - CGFloat(thirdPegDisks.count + 1) * diskHeight)
        } else if from == .second && to == .first {
            yToRaise = pegHeight + 0.5 - CGFloat(secondPegDisks.count + 1) * diskHeight
            xToMove = diskRadius * -2
            yToDrop = -1 * (pegHeight + 0.5 - CGFloat(firstPegDisks.count + 1) * diskHeight)
        } else if from == .second && to == .third {
            yToRaise = pegHeight + 0.5 - CGFloat(secondPegDisks.count + 1) * diskHeight
            xToMove = diskRadius * 2
            yToDrop = -1 * (pegHeight + 0.5 - CGFloat(thirdPegDisks.count + 1) * diskHeight)
        } else if from == .third && to == .second {
            yToRaise = pegHeight + 0.5 - CGFloat(thirdPegDisks.count + 1) * diskHeight
            xToMove = diskRadius * -2
            yToDrop = -1 * (pegHeight + 0.5 - CGFloat(secondPegDisks.count + 1) * diskHeight)
        } else {
            yToRaise = pegHeight + 0.5 - CGFloat(thirdPegDisks.count + 1) * diskHeight
            xToMove = diskRadius * -4
            yToDrop = -1 * (pegHeight + 0.5 - CGFloat(firstPegDisks.count + 1) * diskHeight)
        }
    }
    //
    
    //Initializers
    func generateDisks() {
        
        var scale : CGFloat = 1.0
        for n in 0..<diskCount {
            let diskGeometry = SCNTube(innerRadius: pegRadius,
                                          outerRadius: diskRadius * scale,
                                          height: diskHeight)
            diskGeometry.firstMaterial?.diffuse.contents = UIColor(hue: CGFloat(n) / CGFloat(diskCount),
                                                                   saturation: 1, brightness: 1, alpha: 1)
            let diskNode = SCNNode(geometry: diskGeometry)
            diskNode.position = SCNVector3(diskRadius * -2, diskHeight * CGFloat(n + 1), 0.0)
            rootNode.addChildNode(diskNode)
            firstPegDisks.append(diskNode)
            scale -= 0.2
        }
        
    }
    
    func generatePegs() {
        for index in 0..<3 {
            let pegGeometry = SCNCylinder(radius: pegRadius, height: pegHeight)
            pegGeometry.firstMaterial?.diffuse.contents = UIColor.brown
            let pegNode = SCNNode(geometry: pegGeometry)
            
            var x : CGFloat = 0.0
            switch index {
            case 0 : x -= diskRadius * 2
            case 2 : x += diskRadius * 2
            default : break
            }
            
            pegNode.position = SCNVector3(x, pegHeight / 2 + boardHeight / 2, 0.0)
            rootNode.addChildNode(pegNode)
        }
    }
    
    func generateBoard() {
        let boardGeometry = SCNBox(width: boardWidth,
                                   height: boardHeight,
                                   length: boardLength,
                                   chamferRadius: 0.1)
        
        boardGeometry.firstMaterial?.diffuse.contents = UIColor.brown
        let boardNode = SCNNode(geometry: boardGeometry)
        rootNode.addChildNode(boardNode)
    }
    //----------------------------------------------------------------------------
    
    //Trash
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func tArray(peg : Peg) -> [SCNNode]? {
        switch peg {
        case .first: return firstPegDisks
        case .second: return secondPegDisks
        case .third: return thirdPegDisks
        }
    }
    
    //-----------------------------------------------------
}
