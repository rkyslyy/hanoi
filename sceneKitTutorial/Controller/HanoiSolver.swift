//
//  HanoiSolver.swift
//  sceneKitTutorial
//
//  Created by Roman KYSLYY on 11/15/18.
//  Copyright © 2018 Roman KYSLYY. All rights reserved.
//

import Foundation
import SceneKit

class HanoiSolver {
    
    var smallestDisk = SCNNode()
    
    init() {}
    
    init(smallestDisk : SCNNode) {
        self.smallestDisk = smallestDisk
    }
    
    func getPegAndAmountOnTop(targetRadius : CGFloat, otherOne : ([SCNNode], Peg), otherTwo : ([SCNNode], Peg)) -> (array : [SCNNode], amount : Int, peg : Peg) {
        var result = SCNNode()
        var ret = [SCNNode]()
        var retPeg = Peg.first
        if otherOne.0.count > 0 {
            result = otherOne.0.last!
        }
        if otherTwo.0.count > 0 {
            result = otherTwo.0.last!
        }
        for disk in otherOne.0 {
            if (disk.geometry as! SCNTube).outerRadius < (result.geometry as! SCNTube).outerRadius {
                result = disk
            }
        }
        for disk in otherTwo.0 {
            if (disk.geometry as! SCNTube).outerRadius < (result.geometry as! SCNTube).outerRadius {
                result = disk
            }
        }
        print("Target radius = \(targetRadius)")
        print("Smallest radius = \((result.geometry as! SCNTube).outerRadius)")
        var amount = 0
        for (n, disk) in otherOne.0.reversed().enumerated() {
//            print((disk.geometry as! SCNTube).outerRadius)
//            print(targetRadius)
//            print("")
            if (disk.geometry as! SCNTube).outerRadius < targetRadius && (disk.geometry as! SCNTube).outerRadius >= (result.geometry as! SCNTube).outerRadius {
                result = disk
                ret = otherOne.0
                amount = n
                retPeg = otherOne.1
                print("\((disk.geometry as! SCNTube).outerRadius) is smaller than \(targetRadius)")
                print(targetRadius)
                print("")
            }
        }
        for (n, disk) in otherTwo.0.reversed().enumerated() {
//            print((disk.geometry as! SCNTube).outerRadius)
//            print(targetRadius)
//            print("")
            if (disk.geometry as! SCNTube).outerRadius < targetRadius && (disk.geometry as! SCNTube).outerRadius >= (result.geometry as! SCNTube).outerRadius {
                result = disk
                ret = otherTwo.0
                amount = n
                retPeg = otherTwo.1
                print("\((disk.geometry as! SCNTube).outerRadius) is smaller than \(targetRadius)")
                print(targetRadius)
                print("")
            }
        }
        return (ret, amount, retPeg)
    }
    
    func isTargetClosed(target : [SCNNode]) -> Bool {
        if (target.last!.geometry as! SCNTube).outerRadius == (smallestDisk.geometry as! SCNTube).outerRadius {
            return true
        }
        return false
    }
    
    func findSmallestOnTopAndDest(firstPeg : [SCNNode],
                                  secondPeg : [SCNNode],
                                  thirdPeg : [SCNNode]) -> (Peg, Peg) {
        var smallestDiskPeg : Peg? = nil
        var smallestOpenDisk : SCNNode? = nil
        
        if firstPeg.count != 0 && (firstPeg.last!.geometry as! SCNTube).outerRadius != (smallestDisk.geometry as! SCNTube).outerRadius {
            smallestOpenDisk = firstPeg.last!
            smallestDiskPeg = .first
        }
        if secondPeg.count != 0 && (secondPeg.last!.geometry as! SCNTube).outerRadius != (smallestDisk.geometry as! SCNTube).outerRadius {
            if let disk = smallestOpenDisk {
                if (secondPeg.last!.geometry as! SCNTube).outerRadius < (disk.geometry as! SCNTube).outerRadius {
                    smallestOpenDisk = secondPeg.last!
                    smallestDiskPeg = .second
                }
            } else {
                smallestOpenDisk = secondPeg.last!
                smallestDiskPeg = .second
            }
        }
        if thirdPeg.count != 0 && (thirdPeg.last!.geometry as! SCNTube).outerRadius != (smallestDisk.geometry as! SCNTube).outerRadius {
            if let disk = smallestOpenDisk {
                if (thirdPeg.last!.geometry as! SCNTube).outerRadius < (disk.geometry as! SCNTube).outerRadius {
                    smallestOpenDisk = thirdPeg.last!
                    smallestDiskPeg = .third
                }
            } else {
                smallestOpenDisk = thirdPeg.last!
                smallestDiskPeg = .third
            }
        }
        
        var destination : Peg
        if smallestDiskPeg == .first {
            if secondPeg.count == 0 || (secondPeg.last!.geometry as! SCNTube).outerRadius > (smallestOpenDisk!.geometry as! SCNTube).outerRadius {
                destination = .second
            } else {
                destination = .third
            }
        } else if smallestDiskPeg == .second {
            if firstPeg.count == 0 || (firstPeg.last!.geometry as! SCNTube).outerRadius > (smallestOpenDisk!.geometry as! SCNTube).outerRadius {
                destination = .first
            } else {
                destination = .third
            }
        } else {
            if firstPeg.count == 0 || (firstPeg.last!.geometry as! SCNTube).outerRadius > (smallestOpenDisk!.geometry as! SCNTube).outerRadius {
                destination = .first
            } else {
                destination = .second
            }
        }
        return (smallestDiskPeg!, destination)
    }
}
